package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.ConnectionProvider;

@SuppressWarnings("serial")
@WebServlet("/" + ListeRencontres.SERVLET_PUBLIC_NAME)
public class ListeRencontres extends HttpServlet
{
	protected static final String SERVLET_PUBLIC_NAME = "Rencontres";
	
	
    private Connection connection = null;
    
    @Override
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init();
        try {
            connection = ConnectionProvider.instance().getConnection();
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    	throws ServletException, IOException
    {
    	
    	
    	resp.sendRedirect(SERVLET_PUBLIC_NAME);
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException
    {
    	resp.setContentType("text/html;charset=UTF-8");

        final PrintWriter out = resp.getWriter();
        
        final String sort = getParamSafe(req, "sort", String::toLowerCase, "jour", "num_match", "eq1", "eq2", "sc1", "sc2");
        final String direction = getParamSafe(req, "dir", String::toUpperCase, "ASC", "DESC");
        
        PreparedStatement rencontresSt = null;
        ResultSet rencontres = null;
        
        try (
    		PreparedStatement equipesSt = connection.prepareStatement("SELECT * FROM equipes");
    		ResultSet equipes = equipesSt.executeQuery();
        ) {
        	rencontresSt = connection.prepareStatement(String.format("SELECT * FROM rencontres ORDER BY %s %s", sort, direction));
        	
            rencontres = rencontresSt.executeQuery();
            
            Map<Integer, String> teams = new HashMap<>();
            while (equipes.next()) {
                teams.put(equipes.getInt("num_equipe"), equipes.getString("nom_equipe"));
            }
            
            out.println("<body><center>");
            
            out.println("<form action=\""+SERVLET_PUBLIC_NAME+"\" method=\"POST\">");
            
            out.println("<label for=\"num_match\">Numéro du match</label>");
            out.println("<input name=\"num_match\" type=\"number\" value=\"0\"/>");
            
            out.println("<label for=\"jour\">Date du match</label>");
            out.println("<input name=\"jour\" type=\"date\"/><br/>");
            
            out.println("<label for=\"eq1\">Equipe 1</label>");
            out.println("<input name=\"eq1\" type=\"number\"/>");
            out.println("<label for=\"sc1\">Score équipe 1</label>");
            out.println("<input name=\"sc1\" type=\"number\"/><br/>");

            out.println("<label for=\"eq2\">Equipe 2</label>");
            out.println("<input name=\"eq2\" type=\"number\"/>");
            out.println("<label for=\"sc2\">Score équipe 2</label>");
            out.println("<input name=\"sc2\" type=\"number\"/><br/>");
            
            out.println("<input type=\"submit\" value=\"Ajouter\"/>");
            out.println("</form>");
            
            
            htmlHead(out);

            out.println("<table>");
            printTableHeader(out);

            while (rencontres.next()) {
                printTableRow(out, rencontres, teams);
            }
            
            out.println("</table></center></body>");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        } finally {
    		try {
				rencontresSt.close();
				rencontres.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
        out.println("</center></body>");
    }
	private String getParamSafe(HttpServletRequest req, String param, Function<String, String> transform, String... validValues) {
		String value = req.getParameter(param);

		if (validValues.length == 0) {
			return transform.apply(value);
		}
		
		final String defVal = transform.apply(validValues[0]);
		
        if (value == null || value.length() == 0) {
        	return defVal;
        }
        
        value = transform.apply(value);
        
        if (!Arrays.asList(validValues).contains(value)) {
        	return defVal;
        }
        
		return value;
	}

	private void printTableRow(final PrintWriter out, ResultSet rencontres, Map<Integer, String> teams)
			throws SQLException {
		final String date = rencontres.getString("jour");
		final int numero = rencontres.getInt("num_match");
		
		final int eq1 = rencontres.getInt("eq1");
		final String eqA = teams.get(eq1);

		final int eq2 = rencontres.getInt("eq2");
		final String eqB = teams.get(eq2);
		
		final int sc1 = rencontres.getInt("sc1");
		final int sc2 = rencontres.getInt("sc2");
		
		final String score = String.format("%02d &mdash; %02d", sc1, sc2);
		
		out.println("<tr>");
		out.println(String.format("<td>%d</td>", numero));
		out.println(String.format("<td>%s</td>", date));
		out.println(String.format("<td>%04d (%s)</td>", eq1, eqA));
		out.println(String.format("<td colspan=\"2\">%s</td>", score));
		out.println(String.format("<td>%04d (%s)</td>", eq2, eqB));
		out.println("</tr>");
	}

	private void printTableHeader(final PrintWriter out) {
		out.println("<tr>");
		tableHeader(out, "num_match", "#");
		tableHeader(out, "jour", "Jour");
		tableHeader(out, "eq1", "Equipe A");
		tableHeader(out, "sc1", "Score A");
		tableHeader(out, "sc2", "Score B");
		tableHeader(out, "eq2", "Equipe B");
		out.println("</tr>");
	}

	private void htmlHead(final PrintWriter out) {
		out.println("<!doctype html>");
		out.println("<head>");
		out.println("<title>Liste rencontres sportives</title>");
		out.println("<style>th, td {border:solid 0.1em black;padding:0.3em;}</style>");
		out.println("<style>a {text-decoration:none;}</style>");
		out.println("</head>");
	}

	private void tableHeader(final PrintWriter out, String column, String title) {
		out.println(String.format("<th>%1$s&nbsp;<a href=\"?sort=%2$s&dir=asc\">&darr;</a><a href=\"?sort=%2$s&dir=desc\">&uarr;</a></th>", title, column));
	}
}